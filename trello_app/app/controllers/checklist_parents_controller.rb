class ChecklistParentsController < ApplicationController
  before_action :set_checklist_parent, only: [:show, :update, :destroy]

  # GET /checklist_parents
  def index
    @checklist_parents = ChecklistParent.all

    render json: @checklist_parents
  end

  # GET /checklist_parents/1
  def show
    render json: @checklist_parent
  end

  # POST /checklist_parents
  def create
    @checklist_parent = ChecklistParent.new(checklist_parent_params)

    if @checklist_parent.save
      render json: @checklist_parent, status: :created, location: @checklist_parent
    else
      render json: @checklist_parent.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /checklist_parents/1
  def update
    if @checklist_parent.update(checklist_parent_params)
      render json: @checklist_parent
    else
      render json: @checklist_parent.errors, status: :unprocessable_entity
    end
  end

  # DELETE /checklist_parents/1
  def destroy
    @checklist_parent.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_checklist_parent
      @checklist_parent = ChecklistParent.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def checklist_parent_params
      params.require(:checklist_parent).permit(:card_id, :title)
    end
end
