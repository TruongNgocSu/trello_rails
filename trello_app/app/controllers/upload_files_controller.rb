class UploadFilesController < ApplicationController
  before_action :set_upload_file, only: [:show, :update, :destroy]

  # GET /upload_files
  def index
    @upload_files = UploadFile.all

    render json: @upload_files
  end

  # GET /upload_files/1
  def show
    render json: @upload_file
  end

  # POST /upload_files
  def create
    @upload_file = UploadFile.new(upload_file_params)

    if @upload_file.save
      render json: @upload_file, status: :created, location: @upload_file
    else
      render json: @upload_file.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /upload_files/1
  def update
    if @upload_file.update(upload_file_params)
      render json: @upload_file
    else
      render json: @upload_file.errors, status: :unprocessable_entity
    end
  end

  # DELETE /upload_files/1
  def destroy
    @upload_file.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_upload_file
      @upload_file = UploadFile.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def upload_file_params
      params.require(:upload_file).permit(:card_id, :file_name, :file_path)
    end
end
