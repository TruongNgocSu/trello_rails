class ChecklistParent < ApplicationRecord
  belongs_to :card
  has_many :checklist
end
