class Card < ApplicationRecord
  has_many :comment
  has_many :action
  has_many :checklist_parent
  has_many :upload_file
  has_and_belongs_to_many :user
end
