class User < ApplicationRecord
  has_secure_password

  has_many :comment, dependent: :destroy
  has_many :action, dependent: :destroy
  has_and_belongs_to_many :user
end
