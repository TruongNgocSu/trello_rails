Rails.application.routes.draw do
  resources :user_cards
  resources :checklists
  resources :checklist_parents
  resources :upload_files
  resources :actions
  resources :comments
  resources :cards
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
