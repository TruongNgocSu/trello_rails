# frozen_string_literal: true

User.create(email: 'admin@gmail.com',
            password: :password,
            name: 'admin')
