class CreateCards < ActiveRecord::Migration[6.1]
  def change
    create_table :cards do |t|
      t.string :title
      t.boolean :status
      t.text :description
      t.integer :position
      t.bigint :creator_id, null: false

      t.timestamps
    end
    add_foreign_key :cards, :users, column: :creator_id, primary_key: :"id", on_delete: :cascade
  end
end
