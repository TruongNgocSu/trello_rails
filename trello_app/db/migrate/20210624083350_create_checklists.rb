class CreateChecklists < ActiveRecord::Migration[6.1]
  def change
    create_table :checklists do |t|
      t.bigint :checklist_parent_id, null:false
      t.text :content
      t.boolean :status

      t.timestamps
    end
    add_foreign_key :checklists, :checklist_parents, on_delete: :cascade
  end
end
