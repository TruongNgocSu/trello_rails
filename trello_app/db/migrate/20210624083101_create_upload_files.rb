class CreateUploadFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :upload_files do |t|
      t.bigint :card_id, null: false
      t.string :file_name
      t.string :file_path

      t.timestamps
    end
    add_foreign_key :upload_files, :cards, on_delete: :cascade
  end
end
