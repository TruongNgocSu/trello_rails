class CreateChecklistParents < ActiveRecord::Migration[6.1]
  def change
    create_table :checklist_parents do |t|
      t.bigint :card_id, null: false
      t.text :title

      t.timestamps
    end
    add_foreign_key :checklist_parents, :cards, on_delete: :cascade
  end
end
