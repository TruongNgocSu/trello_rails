class CreateActions < ActiveRecord::Migration[6.1]
  def change
    create_table :actions do |t|
      t.bigint :card_id, null: false
      t.bigint :user_id, null: false
      t.string :action
      t.text :content

      t.timestamps
    end
    add_foreign_key :actions, :users, on_delete: :cascade
    add_foreign_key :actions, :cards, on_delete: :cascade
  end
end
